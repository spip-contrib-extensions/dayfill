<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'dayfill_nom'         => "DayFill - Gestionnaire d'activités",
	'dayfill_slogan'      => "Gérer des activités liées à des projets",
	'dayfill_description' => "DayFill (ou Défile en français, comme «le temps qui défile...») permet de saisir des activités (des actions) dans le cadre d'un projet.",
);

?>
