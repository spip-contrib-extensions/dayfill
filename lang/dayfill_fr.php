<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'affichage_liste'                   => 'Liste',
	'affichage_calendrier'              => 'Calendrier',
	'ajouter_activite'                  => 'Ajouter une activité',

	// D
	'dayfill_titre'                     => 'DayFill',

	// C
	'cfg_exemple'                       => 'Exemple',
	'cfg_exemple_explication'           => 'Explication de cet exemple',
	'cfg_titre_parametrages'            => 'Paramétrages',

	// E
	'erreur_date_fin_plus_ancien'       => "L'heure de fin ne doit pas être antérieure à l'heure de début",
	'erreur_saisie_invalide'            => "Votre saisie contient des erreurs !",
	'erreur_saisie_nb_h_passees_diff'   => "Le nombre que vous avez saisi ne correspond pas à l'heure de début et de fin. Cela devrait être '@indic@'",
	'erreur_saisie_nb_h_passees_vide'   => "Vous n'avez pas saisi d'heures passées alors que vous avez une heure de début et de fin. Pour indication, vous devait saisir '@indic@'",
	'explication_activite_par_defaut'   => "Précisez le type d'activité sélectionné par défaut",
	'explication_detail_activite'       => "Décrivez l'activité effectuée",
	'explication_duree'                 => "Indiquez la date de l'activité, en précisant les horaires de début et de fin, puis ajustez la durée calculée",
	'explication_facture_activite'      => "Sélectionnez parmi les factures valides de @organisation@, celle sur laquelle imputer cette activité",
	'explication_groupe_mots_activites' => "Sélectionnez le groupe de mots-clé utilisé pour définir les activités",
	'explication_id_auteur'             => "Choisissez l'auteur parmi les collaborateurs potentiels.",
	'explication_profil_admin'          => "Saisie d'activités et attribution des factures",
	'explication_profil_developpeur'    => "Saisie d'activités liées à un faible nombre de projet et peu souvent dans la journée",
	'explication_profil_operateur'      => "Saisie en continu à partir des fiches papier",
	'explication_profil_ticketteur'     => "Saisie d'activités liées à un grand nombre de projets et clients, plusieurs fois par jour",
	'explication_quel_projet'           => "Sélectionnez un des projets actifs du client sélectionné",
	'explication_quelle_organisation'   => "Sélectionnez le client pour qui l'activité a été effectuée",
	'explication_type_activite'         => "Sélectionner le type qui correspond le mieux à cette activité",

	// I
	'info_profil_change'                => "Profil d'utilisation changé !",

	// L
	'label_activite_par_defaut'         => "Type d'activité par défaut",
	'label_auteurs_potentiels'          => "Collaborateurs potentiels",
	'label_auteurs_projet'              => "Déjà attribués au projet",
	'label_changer_profil'              => "Changer la disposition du formulaire selon votre profil",
	'label_date_action'                 => "Date",
	'label_decrire_action'              => "Décrivez le type d'action effectuée",
	'label_detail_activite'             => "Détail de l'activité",
	'label_duree'                       => "Date et durée de l'activité",
	'label_facture_activite'            => "Facture à imputer",
	'label_groupe_mots_activites'       => "Groupe de mots-clé pour les activités",
	'label_heure_a'                     => " à : ",
	'label_heure_de'                    => "De : ",
	'label_heure_debut'                 => "Début",
	'label_heure_fin'                   => "Fin",
	'label_id_auteur'                   => "Auteur de l'activité",
	'label_id_projet'                   => "ID",
	'label_profil_admin'                => "Administrateur",
	'label_profil_developpeur'          => "Développeur",
	'label_profil_operateur'            => "Opérateur",
	'label_profil_ticketteur'           => "Ticketteur",
	'label_quel_client'                 => "Pour quel client ?",
	'label_quel_projet'                 => "Dans le cadre de quel projet ?",
	'label_quel_temps_passe'            => "Combien de temps &agrave; facturer ?",
	'label_quel_type_action'            => "Quel type d'action a été effectué ?",
	'label_quelle_organisation'         => "Pour quel client ?",
	'label_temps_facture'               => "Combien de temps facturé ?",
	'label_temps_passe'                 => "Combien de temps passé ?",
	'label_type_action'                 => "Type d'action",
	'label_type_activite'               => "Type d'activité",

	// T
	'titre_page_configurer_dayfill'     => 'Configuer le gestionnaire d\'activités',

);

?>
